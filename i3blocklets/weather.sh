#!/bin/bash

[ -z "$button" ] && curl 'wttr.in?0&T&Q&format="%c+%f"' && exit 0

x="$1"; y="$2"
let 'x = x - 610' # Input will look like "clickpos-relpos"
let 'y = y - 266' # Input will look like "clickpos-relpos"

[ "$button" == 1 ]  && feh --title 'weather' --class=float \
                           --draw-tinted -B '#555555' -x -N \
                           --geometry +"$x"+"$y" --on-last-slide quit \
                           'https://wttr.in/_tqp1.png'

curl 'wttr.in?0&T&Q&format="%c+%f"'
