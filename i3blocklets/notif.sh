#!/bin/bash

[ "$button" == 1 ] && kill -s USR1 $(pidof deadd-notification-center) \
  && printf '{"full_text: "🔔", "_muted: %d"}\n' $_muted

# TODO: mute / unmute notifs
(( $_muted == 1 ))  && printf '{"full_text":"🔔", "_muted":%d}\n' $_muted $(( (_muted + 1) % 2 )) \
    || printf '{"full_text":"🔕", "_muted":%d}\n' $_muted $(( (_muted + 1) % 2 ))


