#!/bin/bash
# Source: https://github.com/vivien/i3blocks

[ $button == 2 ] && [ -n $button ] && xfce4-power-manager-settings

read -d '\t' state lvl rate < <(acpi -b | head -n 1 \
                                  | awk -F ": |, " \
                                        ' { printf "%s\t%s\t%s\n", $2, $3, $4 } ')

# Remove 'remaining' from end of rate variable
rate="${rate%%remaining}"

# Full and short texts
case "$state" in
  'Discharging')
    [ -n $button ] && [ $button != 2 ] \
      && echo "🔋 $lvl $(date -d "$rate" +'%-Hh %-Mm')" \
      || echo "🔋 $lvl";;
  'Charging')
    [ -n $button ] && [ -n $button != 2] \
      && echo "⚡🔋$lvl $(date -d "$rate" +'%-Hh %-Mm')" \
      || echo "⚡🔋 $lvl";;
  'Full') echo "✅🔋 $lvl";;
  'Unknown') echo "❔🔋 $lvl";;
esac

# Set urgent flag below 5% or use orange below 20%
[ "${lvl%?}" -le 5 ] && exit 33
[ "${lvl%?}" -le 20 ] && echo "#FF8000"

exit 0
