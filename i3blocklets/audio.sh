#!/bin/bash

[ $button == 1 ] && pavucontrol

# Could potentially create a race condition b/c volume.sh
# calls this script. Means that 2 notifications are
# likely popping up in quick succession (one from
# the code that runs after this line, another from
# the code that runs *because* of this line)
[ $button == 3 ] && volume.sh 0

lvl=$(pulsemixer --get-volume | cut -d ' ' -f 1)
mute=$(pulsemixer --get-mute)

if (( $mute == 1 )); then icon=🔇
elif (( $lvl > 66 )); then icon=🔊
elif (( $lvl > 33 )); then icon=🔉
elif (( $lvl > 0 )); then icon=🔈
else icon=🔇
fi

echo $icon
