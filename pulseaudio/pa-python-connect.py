#!/usr/bin/env python3

# Instructions
# If this program is run alongside pulseaudio, it will send notifications
# anytime the volume or mute level changes. 

# https://stackoverflow.com/a/33776607
# https://www.freedesktop.org/wiki/Software/PulseAudio/Documentation/Developer/Clients/DBus/

import os
import sys
import dbus
import math
from dbus.mainloop.glib import DBusGMainLoop
from gi.repository import GLib

# Shorthand for getting dbus properties
def prop(pulseobj, prop, dbus_interface):
    return pulseobj.Get(prop, dbus_interface, 
                        dbus_interface="org.freedesktop.DBus.Properties")

# Gets the volume of an audio sink.
def sink_get_volume(snk, vol=None):
    baseVol = snk.Get('org.PulseAudio.Core1.Device', 'BaseVolume')
    if vol == None:
        vol = snk.Get('org.PulseAudio.Core1.Device', 'Volume')[0]
    return round(100*(vol / baseVol))

# Run whenever volume of audio sink changes
def handle_vol_update(snk, vol):
    per = sink_get_volume(snk, vol[0])
    print("Volume changed to: \n", per)
    lvl = 'low' if per in range(0, 33) \
          else 'medium' if per in range(33, 66) \
          else 'high'
    icon = f"volume-level-{lvl}"
    os.system(f'''
        notify-send.py "Volume: {per}" \
                       --replaces-process volumenotif \
                       -a "Volume Control" -i preferences-sound \
                       --hint "string:image-path:{icon}" \
                              "bool:transient:true" &
    ''')

# Run whenever audio sink is muted/unmuted
def handle_mute_update(snk, muted):
    print("Volume has been %s." % ("muted" if muted else "unmuted"))
    vol = sink_get_volume(snk)
    lvl = 'low' if vol in range(0, 33) \
          else 'medium' if vol in range(33, 66) \
          else 'high'
    summary, icon = (f'Volume: {vol}', 'volume-level-muted') if not muted else \
            ('Volume: Muted', f'volume-level-{lvl}')
    os.system(f'''
        notify-send.py "{summary}" \
                       --replaces-process volumenotif \
                       -a "Volume Control" -i preferences-sound \
                       --hint "string:image-path:{icon}" \
                              "bool:transient:true" &
    ''')

# Locate the pulseaudio address
def pulse_bus_address():
    if 'PULSE_DBUS_SERVER' in os.environ:
        address = os.environ['PULSE_DBUS_SERVER']
    else:
        bus = dbus.SessionBus()
        server_lookup = bus.get_object("org.PulseAudio1", "/org/pulseaudio/server_lookup1")
        address = server_lookup.Get("org.PulseAudio.ServerLookup1", "Address", dbus_interface="org.freedesktop.DBus.Properties")
    return address

# Setup Main Loop
DBusGMainLoop(set_as_default=True)
loop = GLib.MainLoop()

pulse_bus = dbus.connection.Connection(pulse_bus_address())
pulse_core = pulse_bus.get_object(object_path="/org/pulseaudio/core1")
prop_core = lambda p: prop(pulse_core, "org.PulseAudio.Core1", p)
name = prop_core("Name")
sinks = prop_core("Sinks")
def_sink = pulse_bus.get_object("org.PulseAudio.Core1", sinks[0])

# Initial Prints
print("Successfully connected to", name, "!")
print("Sinks: \n", prop_core("Sinks"))

# Setup Signals to listen for in main loop
pulse_core.ListenForSignal('org.PulseAudio.Core1.Device.StateUpdated', dbus.Array(signature='o'), dbus_interface='org.PulseAudio.Core1')
pulse_core.ListenForSignal('org.PulseAudio.Core1.Device.VolumeUpdated', dbus.Array(signature='o'), dbus_interface='org.PulseAudio.Core1')
pulse_core.ListenForSignal('org.PulseAudio.Core1.Device.MuteUpdated', dbus.Array(signature='o'), dbus_interface='org.PulseAudio.Core1')

# Added handlers for signals
# TODO: only run filter for default sink
pulse_bus.add_signal_receiver(lambda v: handle_vol_update(def_sink, v), 'VolumeUpdated')
pulse_bus.add_signal_receiver(lambda m: handle_mute_update(def_sink, m), 'MuteUpdated')

# Run Main Loop
loop.run()
