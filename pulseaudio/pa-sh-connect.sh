#!/bin/bash

address=$(dbus-send --print-reply=literal \
                    --dest=org.PulseAudio1 /org/pulseaudio/server_lookup1 \
                    org.freedesktop.DBus.Properties.Get \
                    string:org.PulseAudio.ServerLookup1 string:Address \
            | awk '{ print $2 }')

echo "server address is"
echo $address
