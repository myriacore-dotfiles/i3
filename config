# i3 config file (v4)
# Please see http://i3wm.org/docs/userguide.html for a complete reference!

# Set mod key (Mod1=<Alt>, Mod4=<Super>)
set $mod Mod4

# set default desktop layout (default is tiling)
# workspace_layout tabbed <stacking|tabbed>

# Configure border style <normal|1pixel|pixel xx|none|pixel>
default_border pixel 1
default_floating_border normal

# Hide borders
hide_edge_borders none

# change borders
bindsym $mod+u border none
bindsym $mod+y border pixel 1
bindsym $mod+n border normal

# Font for window titles. Will also be used by the bar unless a different font
# is used in the bar {} block below.
font xft:URWGothic-Book 11

# Use Mouse+$mod to drag floating windows
floating_modifier $mod

# start a terminal
bindsym $mod+Return exec xfce4-terminal --drop-down

# kill focused window
bindsym $mod+Shift+q kill

# open/close notification shade
bindsym $mod+Tab exec --no-startup-id kill -s USR1 $(pidof deadd-notification-center)

################################################################################################
## sound-section - DO NOT EDIT if you wish to automatically upgrade Alsa -> Pulseaudio later! ##
################################################################################################

# exec --no-startup-id volumeicon
# bindsym $mod+Ctrl+m exec terminal -e 'alsamixer'
# exec --no-startup-id pulseaudio --start
# exec --no-startup-id pa-applet

bindsym $mod+Ctrl+m exec pavucontrol
bindsym XF86AudioRaiseVolume exec --no-startup-id ~/.bin/volume.sh +10
bindsym XF86AudioLowerVolume exec --no-startup-id ~/.bin/volume.sh -10
bindsym XF86AudioMute exec --no-startup-id ~/.bin/volume.sh 0

# # Below is an experimental attempt to make volume popups appear faster
# # than the plain shell script method. A python script monitors the pulseaudio
# # DBUS library for volume changes, and then sends a notif. 
# bindsym $mod+Ctrl+m exec --no-startup-id ~/.i3/pulseaudio/pa-python-connect.py
# bindsym XF86AudioRaiseVolume exec --no-startup-id pulsemixer --change-volume +10
# bindsym XF86AudioLowerVolume exec --no-startup-id pulsemixer --change-volume -10
# bindsym XF86AudioMute exec --no-startup-id pulsemixer --toggle-mute

# exec --no-startup-id ~/.bin/pa-virtual-cable-setup.sh # Create audio cables xP

################################################################################################

######### Screen DPI and Screen brightness controls ##############
bindsym XF86MonBrightnessUp exec ~/.bin/backlight.sh +10
bindsym XF86MonBrightnessDown exec ~/.bin/backlight.sh -10

exec xrandr --dpi 120

######### Start Applications ########

# bindsym $mod+Ctrl+b exec terminal -e 'bmenu'
# Settings mode, added By me, replaces the bmenu!
# bindsym $mod+Ctrl+b mode "$mode_settings"
set $mode_settings system_(m)onitor, (l)og_viewer, (s)ettings
mode "$mode_settings" {
    bindsym m exec gnome-system-monitor, mode "default"
    bindsym l exec gnome-system-log, mode "default"
    bindsym s exec manjaro-settings-manager, mode "default"

    # exit settings mode: "Enter" or "Escape"
    bindsym Return mode "default"
    bindsym Escape mode "default"
}

# Project mode, added by me!
# bindsym $mod+p mode "$mode_project"
# set $mode_project project_screen: (l)aptop_screen_only, (m)irror, extend_(u)p, extend_(d)own, extend_(l)eft, extend_(r)ight, d(e)sk_setup, (c)ustom
# mode "$mode_project" {
#     bindsym m exec --no-startup-id ~/.i3/arandr/duplicate-screen.sh, mode "default"
#     bindsym u exec --no-startup-id ~/.i3/arandr/extend-up.sh, mode "default"
#     bindsym l exec --no-startup-id ~/.i3/arandr/extend-left.sh, mode "default"
#     bindsym r exec --no-startup-id ~/.i3/arandr/extend-right.sh, mode "default"
#     bindsym d exec --no-startup-id ~/.i3/arandr/extend-down.sh, mode "default"
#     bindsym e exec --no-startup-id ~/.i3/arandr/desk-setup.sh, mode "default"
#     bindsym l exec ~/.i3/arandr/laptop-screen-only.sh, mode "default"
#     bindsym c exec arandr, mode "default"

#     # exit project mode: "enter" or "escape"
#     bindsym Return mode "default"
#     bindsym Escape mode "default"
# }

bindsym $mod+F2 exec google-chrome-stable
bindsym $mod+F3 exec nemo
bindsym $mod+F5 exec terminal -e 'mocp'
bindsym $mod+t exec --no-startup-id pkill picom
bindsym $mod+Ctrl+t exec --no-startup-id picom --experimental-backends -b >> /tmp/picom 2>&1
# bindsym $mod+Shift+d --release exec "systemctl --user restart deadd-notification-center"
bindsym Print exec --no-startup-id flameshot gui
bindsym $mod+Print exec ~/.bin/record-screen; mode "$mode_screenrecord"
set $mode_screenrecord WIN+PRINT: Finish Recording, WIN+ESC: Cancel Recording
# TODO: Convert into sysbar widget
mode "$mode_screenrecord" {
    # Screenrecord bindings
    bindsym $mod+Print exec "kill -INT $(pgrep record-screen)", mode "default"
    bindsym $mod+Escape exec "kill -TERM $(pgrep record-screen)", mode "default"
    # Other system bindings (so I can use my system mostly-normally)
    bindsym $mod+period exec bash ~/.texpander/.bin/texpander.sh # text replacements
    bindsym $mod+Return exec xfce4-terminal --drop-down
}

bindsym $mod+Shift+h exec xdg-open /usr/share/doc/manjaro/i3_help.pdf
bindsym $mod+Shift+x --release exec --no-startup-id xkill
bindsym $mod+period exec bash ~/.texpander/.bin/texpander.sh # text replacements

# focus_follows_mouse no

# change focus
bindsym $mod+j focus left
bindsym $mod+k focus down
bindsym $mod+l focus up
bindsym $mod+semicolon focus right

# alternatively, you can use the cursor keys:
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# move focused window
bindsym $mod+Shift+j move left
bindsym $mod+Shift+k move down
bindsym $mod+Shift+l move up
bindsym $mod+Shift+semicolon move right

# alternatively, you can use the cursor keys:
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

# workspace back and forth (with/without active container)
workspace_auto_back_and_forth yes
bindsym $mod+b workspace back_and_forth
bindsym $mod+Shift+b move container to workspace back_and_forth; workspace back_and_forth

# split orientation
bindsym $mod+h split h;exec notify-send 'tile horizontally'
bindsym $mod+v split v;exec notify-send 'tile vertically'
bindsym $mod+q split toggle

# toggle fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

# toggle sticky
bindsym $mod+Shift+s sticky toggle

# focus the parent container
bindsym $mod+a focus parent

# move the currently focused window to the scratchpad
bindsym $mod+Shift+minus move scratchpad

# Show the next scratchpad window or hide the focused scratchpad window.
# If there are multiple scratchpad windows, this command cycles through them.
bindsym $mod+minus scratchpad show

# toggle tiling / floating
bindsym $mod+Shift+plus floating toggle

# change focus between tiling / floating windows
bindsym $mod+plus focus mode_toggle

#navigate workspaces next / previous
bindsym $mod+Ctrl+Right workspace next
bindsym $mod+Ctrl+Left workspace prev

# Workspace names
# to display names or symbols instead of plain workspace numbers you can use
# something like: set $ws1 1:mail
#                 set $ws2 2:
# wst     :: messaging
# ws1     :: web browser
# ws2     :: file browser
# ws3     :: file editing (code, images, etc)
# ws4     :: virtual machines & remote desktops
# ws5     :: games
# ws6-ws9 :: free space
# ws0     :: terminal
# set $wst "~: 💬"
set $wst "💬"
set $ws1 "1: 🌐"
set $ws2 "2: 📁"
# set $ws3 "3:   "
set $ws3 "3: 📝"
set $ws4 "4: 🖥️"
set $ws5 "5: 🎮"
set $ws6 "6"
set $ws7 "7"
set $ws8 "8"
set $ws9 "9"
set $ws0 "0: "

# Workspace layout settings
workspace_layout tabbed # Options = default | stacked | tabbed

# switch to workspace; layout exceptions
bindsym $mod+grave workspace $wst
bindsym $mod+1 workspace $ws1
bindsym $mod+2 workspace $ws2
bindsym $mod+3 workspace $ws3
bindsym $mod+4 workspace $ws4
bindsym $mod+5 workspace $ws5
bindsym $mod+6 workspace $ws6
bindsym $mod+7 workspace $ws7
bindsym $mod+8 workspace $ws8
bindsym $mod+9 workspace $ws9
bindsym $mod+0 workspace $ws0

# Move focused container to workspace
bindsym $mod+Ctrl+grave move container to workspace $wst
bindsym $mod+Ctrl+1 move container to workspace $ws1
bindsym $mod+Ctrl+2 move container to workspace $ws2
bindsym $mod+Ctrl+3 move container to workspace $ws3
bindsym $mod+Ctrl+4 move container to workspace $ws4
bindsym $mod+Ctrl+5 move container to workspace $ws5
bindsym $mod+Ctrl+6 move container to workspace $ws6
bindsym $mod+Ctrl+7 move container to workspace $ws7
bindsym $mod+Ctrl+8 move container to workspace $ws8
bindsym $mod+Ctrl+9 move container to workspace $ws9
bindsym $mod+Ctrl+0 move container to workspace $ws0

# Move to workspace with focused container
bindsym $mod+Shift+grave move container to workspace $wst; workspace $wst
bindsym $mod+Shift+1 move container to workspace $ws1; workspace $ws1
bindsym $mod+Shift+2 move container to workspace $ws2; workspace $ws2
bindsym $mod+Shift+3 move container to workspace $ws3; workspace $ws3
bindsym $mod+Shift+4 move container to workspace $ws4; workspace $ws4
bindsym $mod+Shift+5 move container to workspace $ws5; workspace $ws5
bindsym $mod+Shift+6 move container to workspace $ws6; workspace $ws6
bindsym $mod+Shift+7 move container to workspace $ws7; workspace $ws7
bindsym $mod+Shift+8 move container to workspace $ws8; workspace $ws8
bindsym $mod+Shift+9 move container to workspace $ws9; workspace $ws9
bindsym $mod+Shift+0 move container to workspace $ws0; workspace $ws0
# Move workspaces to display outputs
# mod1 ::= left alt, right alt, left meta
bindsym $mod+mod1+Up move workspace to output up
bindsym $mod+mod1+Down move workspace to output down
bindsym $mod+mod1+Left move workspace to output left
bindsym $mod+mod1+Right move workspace to output right

# Open applications on specific workspaces
# chat apps
assign [class="discord"] $wst
assign [class="Riot"] $wst
assign [class="zoom"] $wst
assign [class="obs" title=".* Projector .*"] $wst
# web browsers
assign [class="^Google-chrome$"] $ws1
assign [class="firefox"] $ws1
# file browser
assign [class="^Nemo$"] $ws2
# editing
assign [class="libreoffice"] $ws3
assign [instance="libreoffice"] $ws3
assign [class="Lazpaint"] $ws3
assign [class="Gimp"] $ws3
assign [class="Inkscape"] $ws3
assign [class="Olive"] $ws3
assign [class="Blender"] $ws3
assign [class="Glade"] $ws3
assign [class="code-oss"] $ws3
assign [class="Emacs"] $ws3
assign [title="FVim"] $ws3
assign [class="obs" title="OBS .* - Profile: .* - Scenes: avio"] $ws3
# vm's / remote desktops
assign [class="VirtualBox Manager"] $ws4
assign [class="VirtualBox Machine"] $ws4
assign [class="VirtualBoxVM"] $ws4
assign [class="VirtualBox"] $ws4
assign [class="Workspacesclient"] $ws4
# games
assign [class="Steam"] $ws5
assign [class="Lutris"] $ws5

# OK so the below lines seemed to cause some badness, so I'm gonna prefer the assigns while referencin the below resources I ran into
# https://www.reddit.com/r/i3wm/comments/5rrynz/cannot_assign_googlechrome_to_a_specific_workspace/ddauapc/
# https://i3wm.org/docs/userguide.html#assign_workspace
# for_window [class="^Google-chrome$"] move to workspace $ws1, workspace --no-auto-back-and-forth $ws1
# for_window [class="Nemo"] move to workspace $ws2, workspace --no-auto-back-and-forth $ws2

# Open specific applications in floating mode
for_window [title="alsamixer"] floating enable border pixel 1
for_window [class="blueman-manager"] floating enable
for_window [title="Bluetooth Devices"] floating enable
for_window [class="calamares"] floating enable border normal
for_window [class="Clipgrab"] floating enable
for_window [title="File Transfer*"] floating enable
for_window [class="Galculator"] floating enable border pixel 1
for_window [class="GParted"] floating enable border normal
for_window [title="i3_help"] floating enable sticky enable border normal
for_window [class="Lightdm-settings"] floating enable
for_window [class="Lxappearance"] floating enable sticky enable border normal
for_window [class="calamares"] floating enable border normal
for_window [class="Clipgrab"] floating enable
for_window [title="File Transfer*"] floating enable
for_window [class="Galculator"] floating enable border pixel 1
for_window [class="GParted"] floating enable border normal
for_window [title="i3_help"] floating enable sticky enable border normal
for_window [class="Lightdm-settings"] floating enable
for_window [class="Lxappearance"] floating enable sticky enable border normal
for_window [class="GParted"] floating enable border normal
for_window [title="i3_help"] floating enable sticky enable border normal
for_window [class="Lightdm-settings"] floating enable
for_window [class="Lxappearance"] floating enable sticky enable border normal
for_window [class="Manjaro-hello"] floating enable
for_window [class="Manjaro Settings Manager"] floating enable border normal
for_window [title="MuseScore: Play Panel"] floating enable
for_window [class="Nitrogen"] floating enable sticky enable border normal
for_window [class="Oblogout"] fullscreen enable
for_window [class="octopi"] floating enable
for_window [title="About Pale Moon"] floating enable
for_window [class="Pamac-manager"] floating enable
for_window [class="Pavucontrol"] floating enable; resize 790 520; move position center
for_window [class="qt5ct"] floating enable sticky enable border normal
for_window [class="Qtconfig-qt4"] floating enable sticky enable border normal
for_window [class="Simple-scan"] floating enable border normal
for_window [class="(?i)System-config-printer.py"] floating enable border normal
for_window [class="Skype"] floating enable border normal
for_window [class="Timeset-gui"] floating enable border normal
for_window [class="(?i)virtualbox"] floating enable border normal
for_window [class="Xfburn"] floating enable
for_window [class="Ulauncher" title="Ulauncher Preferences"] floating enable; resize set 1741 979
for_window [class="flameshot"] floating enable #border normal;
for_window [class="flameshot" instance="flameshot" title="Configuration"] border normal
for_window [class="flameshot" instance="flameshot" title="Open With"] border normal
for_window [class="flameshot" instance="flameshot" title="Upload to Imgur"] border normal
for_window [class="Lazpaint" title="Layers"] floating enable border normal
for_window [class="Lazpaint" title="Color"] floating enable border normal
for_window [class="Gimp" window_type="dialog"] floating enable border normal; move position center
# for_window [class="Gimp" window_role="pop-up"] floating enable border normal; move position center
for_window [class="VirtualBox Machine"] floating enable border normal; resize set 1741 979
for_window [class="zoom" instance="zoom" title="Zoom Meeting ID: .*"] floating disable
for_window [class="zoom" instance="zoom" title="Settings"] floating enable border normal; resize 934 604; move position center
for_window [class="zoom" instance="zoom" window_type="notification"] floating enable border normal;
# for_window [class="zoom" instance="zoom" window_type=""] floating enable border normal;
for_window [instance="feh" title="weather"] sticky enable
for_window [class="Steam" title="Friends List"] floating enable
for_window [class="Steam" title="Steam - News (.*)"] floating enable

# windows that want to float should float! :D
for_window [class="float"] floating enable

# temporary assignments
for_window [class="Erlang" title="Chat"] floating enable

# switch to workspace with urgent window automatically
for_window [urgent=latest] focus

# reload the configuration file
bindsym $mod+Shift+c reload

# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart

# exit i3 (logs you out of your X session)
bindsym $mod+Shift+e exec "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -b 'Yes, exit i3' 'i3-msg exit'"

# Set shut down, restart and locking features
# bindsym $mod+BackSpace mode "$mode_system"
set $mode_system (l)ock, (e)xit, switch_(u)ser, (s)uspend, (h)ibernate, (r)eboot, (Shift+s)hutdown
mode "$mode_system" {
    bindsym l exec --no-startup-id i3exit lock, mode "default"
    bindsym s exec --no-startup-id i3exit suspend, mode "default"
    bindsym u exec --no-startup-id i3exit switch_user, mode "default"
    bindsym e exec --no-startup-id i3exit logout, mode "default"
    bindsym h exec --no-startup-id i3exit hibernate, mode "default"
    bindsym r exec --no-startup-id i3exit reboot, mode "default"
    bindsym Shift+s exec --no-startup-id i3exit shutdown, mode "default"

    # exit system mode: "Enter" or "Escape"
    bindsym Return mode "default"
    bindsym Escape mode "default"
}

# Resize window (you can also use the mouse for that)
bindsym $mod+r mode "resize"
mode "resize" {
        # These bindings trigger as soon as you enter the resize mode
        # Pressing left will shrink the window’s width.
        # Pressing right will grow the window’s width.
        # Pressing up will shrink the window’s height.
        # Pressing down will grow the window’s height.
        bindsym j resize shrink width 5 px or 5 ppt
        bindsym k resize grow height 5 px or 5 ppt
        bindsym l resize shrink height 5 px or 5 ppt
        bindsym semicolon resize grow width 5 px or 5 ppt

        # same bindings, but for the arrow keys
        bindsym Left resize shrink width 10 px or 10 ppt
        bindsym Down resize grow height 10 px or 10 ppt
        bindsym Up resize shrink height 10 px or 10 ppt
        bindsym Right resize grow width 10 px or 10 ppt

        # exit resize mode: Enter or Escape
        bindsym Return mode "default"
        bindsym Escape mode "default"
}

# Lock screen
# bindsym $mod+9 exec --no-startup-id blurlock

# Autostart applications
exec --no-startup-id /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1
exec --no-startup-id nitrogen --restore; sleep 1; picom --experimental-backends -b
exec --no-startup-id ~/.configureX # Configure input devices
exec --no-startup-id deadd-notification-center # For some reason, I have 2 notif daemons running on startup.
#exec --no-startup-id manjaro-hello
# exec_always --no-startup-id ~/.config/polybar/launch.sh manjaro-i3
exec_always --no-startup-id ~/.bin/launch-myriacore-taffybar.sh
exec --no-startup-id nm-applet --indicator
exec --no-startup-id xfce4-power-manager
# exec_always --no-startup-id systemctl --user stop dunst # Have to stop dunst because it has a mind of its own >:(
exec --no-startup-id pamac-tray-appindicator # Won't play nice with my taffybar >:(
exec --no-startup-id clipit
exec --no-startup-id ulauncher --hide-window # Key to open ulauncher is $mod+space
exec discord
exec element-desktop
exec firefox
exec nemo
exec --no-startup-id blueman-applet
# exec_always --no-startup-id sbxkb
# exec --no-startup-id start_conky_maia
# exec --no-startup-id start_conky_green
# exec --no-startup-id xautolock -time 30 -locker blurlock
# exec --no-startup-id xautolock -time 30 -locker "i3exit lock"
# exec --no-startup-id xautolock -time 30 -locker blurlock -corners +00-
exec_always --no-startup-id ff-theme-util
exec_always --no-startup-id fix_xcursor
exec --no-startup-id autorandr --default common
# exec_always --no-startup-id feh --bg-scale ~/Pictures/Myriacore\ -\ Extended\wall-16x9.jpg

# Color palette used for the terminal ( ~/.Xresources file )
# Colors are gathered based on the documentation:
# https://i3wm.org/docs/userguide.html#xresources
set_from_resource $color_bg            background
set_from_resource $color_fg            foreground
set_from_resource $color_active_bg     pointerColorBackground
set_from_resource $color_active_fg     pointerColorForeground
set_from_resource $color_black_dark    color0
set_from_resource $color_black_light   color8
set_from_resource $color_red_dark      color1
set_from_resource $color_red_light     color9
set_from_resource $color_green_dark    color2
set_from_resource $color_green_light   color10
set_from_resource $color_yellow_dark   color3
set_from_resource $color_yellow_light  color11
set_from_resource $color_blue_dark     color4
set_from_resource $color_blue_light    color12
set_from_resource $color_magenta_dark  color5
set_from_resource $color_magenta_light color13
set_from_resource $color_cyan_dark     color6
set_from_resource $color_cyan_light    color14
set_from_resource $color_white_dark    color7
set_from_resource $color_white_light   color15


# Relevant things for i3bar
# bindsym --release Caps_Lock exec pkill -SIGRTMIN+10 i3blocks

# Start i3bar to display a workspace bar (plus the system information i3status if available)
#bar {
#	i3bar_command i3bar --transparency
#	# status_command i3status -c ~/.i3/i3status.conf
#  status_command i3blocks -c ~/.i3/i3blocks.conf
#	position bottom

### please set your primary output first. Example: 'xrandr --output eDP1 --primary'
##	tray_output primary
##	tray_output eDP1

#	bindsym button4 nop
#	bindsym button5 nop
##   font xft:URWGothic-Book 11
#	strip_workspace_numbers yes

#    colors {
#        background #222D31FF
#        statusline #F9FAF9FF
#        separator  #454947FF

##                          border    backgr.   text
#        focused_workspace  #F9FAF9FF #16a085FF #292F34FF
#        active_workspace   #595B5BFF #353836FF #FDF6E3FF
#        inactive_workspace #595B5BFF #222D31FF #EEE8D5FF
#        binding_mode       #16a085FF #2C2C2CFF #F9FAF9FF
#        urgent_workspace   #16a085FF #FDF6E3FF #E5201DFF
#    }
#}

# hide/unhide i3status bar
# bindsym $mod+m bar mode toggle

# Theme colors
# class                   border    backgr.   text      indic  .   child_border
  client.focused          #556064FF #556064FF #80FFF9FF #FDF6E3FF
  client.focused_inactive #2F3D44FF #2F3D44FF #1ABC9CFF #454948FF
  client.unfocused        #2F3D44FF #2F3D44FF #1ABC9CFF #454948FF
  client.urgent           #CB4B16FF #FDF6E3FF #1ABC9CFF #268BD2FF
  client.placeholder      #000000FF #0c0c0cFF #ffffffFF #000000FF 
  client.background       #2B2C2BFF

#############################
### settings for i3-gaps: ###
#############################

# Set inner/outer gaps
gaps inner 14
gaps outer -2

# Additionally, you can issue commands with the following syntax. This is useful to bind keys to changing the gap size.
# gaps inner|outer current|all set|plus|minus <px>
# gaps inner all set 10
# gaps outer all plus 5

# Smart gaps (gaps used if only more than one container on the workspace)
smart_gaps on

# Smart borders (draw borders around container only if it is not the only container on this workspace) 
# on|no_gaps (on=always activate and no_gaps=only activate if the gap size to the edge of the screen is 0)
smart_borders on

# Press $mod+Shift+g to enter the gap mode. Choose o or i for modifying outer/inner gaps. Press one of + / - (in-/decrement for current workspace) or 0 (remove gaps for current workspace). If you also press Shift with these keys, the change will be global for all workspaces.
set $mode_gaps Gaps: (o) outer, (i) inner
set $mode_gaps_outer Outer Gaps: +|-|0 (local), Shift + +|-|0 (global)
set $mode_gaps_inner Inner Gaps: +|-|0 (local), Shift + +|-|0 (global)
# bindsym $mod+Shift+g mode "$mode_gaps"

mode "$mode_gaps" {
        bindsym o      mode "$mode_gaps_outer"
        bindsym i      mode "$mode_gaps_inner"
        bindsym Return mode "default"
        bindsym Escape mode "default"
}
mode "$mode_gaps_inner" {
        bindsym plus  gaps inner current plus 5
        bindsym minus gaps inner current minus 5
        bindsym 0     gaps inner current set 0

        bindsym Shift+plus  gaps inner all plus 5
        bindsym Shift+minus gaps inner all minus 5
        bindsym Shift+0     gaps inner all set 0

        bindsym Return mode "default"
        bindsym Escape mode "default"
}
mode "$mode_gaps_outer" {
        bindsym plus  gaps outer current plus 5
        bindsym minus gaps outer current minus 5
        bindsym 0     gaps outer current set 0

        bindsym Shift+plus  gaps outer all plus 5
        bindsym Shift+minus gaps outer all minus 5
        bindsym Shift+0     gaps outer all set 0

        bindsym Return mode "default"
        bindsym Escape mode "default"
}
